package cucumber.Tests;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.Map;

public class NowyUzytkownikSteps {

    @Before(order = 0)
    public void setUp1(){
        System.out.println("Metoda wykonana przed scenariuszem 1");
    }

    @Before(order = 1)
    public void setUp2(){
        System.out.println("Metoda wykonana przed scenariuszem 2    ");
    }


    @Given("^Użytkownik jest na stronie formularza dodawania nowego użytkownika$")
    public void użytkownikJestNaStronieLFormularza() {
        System.out.println("Jestem na stronie formularza rejestracji nowego użytkownika");
    }

    @When("^Użytkownik wprowadza poprawne dane do formularza$")
    public void użytkownikWprowadzaPoprawneDaneDoFormularza(DataTable dataTable)  {
        Map<String,String> data = dataTable.asMap(String.class, String.class);
        System.out.println("Imie: " + data.get("Imie"));
        System.out.println("Nazwisko: " + data.get("Nazwisko"));
        System.out.println("Miasto: " + data.get("Miasto"));
        System.out.println("Ulica: " + data.get("Ulica"));


    }

    @Then("^Informacja dodaniu użytkownika pojawia się na ekranie$")
    public void informacjaDodaniuUżytkownikaPojawiaSięNaEkranie()  {
        System.out.println("Dodano nowego użytkownika");

    }

    @After(order = 5)
    public void after5(){
        System.out.println("Metoda wykonana po scenariuszu 5");
    }

    @After(order = 2)
    public void after2(){
        System.out.println("Metoda wykonana po scenariuszu 2");
    }
}
