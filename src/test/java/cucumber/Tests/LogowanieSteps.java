package cucumber.Tests;

import cucumber.api.java.en.*;
import org.junit.Assert;

public class LogowanieSteps {

    private Logowanie logowanie;
    private String username;
    private String password;


    @Given("^Użytkownik jest na stronie logowania$")
    public void użytkownikJestNaStronieLogowania() {
        logowanie = new Logowanie();
    }

    @And("^Użytkownik klika w przycisk \"([^\"]*)\"$")
    public void użytkownikKlikaWPrzycisk(String arg0) {
        System.out.println("Klikamy przycisk logowanie");
    }

    @Then("^Użytkownik zostaje zalogowany na stronę domową aplikacji$")
    public void użytkownikZostajeZalogowanyNaStronęDomowąAplikacji() {
        Assert.assertTrue(logowanie.isLoggedIn());
    }

    @And("^Informacja o udanym logowaniu zostanie wyświetlona$")
    public void informacjaOUdanymLogowaniuZostanieWyświetlona() {
        Assert.assertTrue(logowanie.getMsg().equals("Udane logowanie"));
    }


    @But("^Podane haslo jest błędne$")
    public void podaneHasloJestBłędne()  {
        String databaseUsername = logowanie.getCurrentUsername();
        String databasePassword = logowanie.getCurrentPassword();
        Assert.assertFalse(username.equals(databaseUsername) && password.equals(databasePassword));
    }

    @Then("^Użytkownik nie zostaje przeniesiony na stronę domową aplikacji$")
    public void użytkownikNieZostajePrzeniesionyNaStronęDomowąAplikacji() {
        Assert.assertFalse(logowanie.isLoggedIn());
    }

    @And("^Informacja o nieudanym logoweaniu$")
    public void informacjaONieudanymLogoweaniu() {
        Assert.assertTrue(logowanie.getMsg().equals("Nieudane logowanie"));
    }

    @When("^Użytkownik wprowadza nazwę użytkownika \"([^\"]*)\" i haslo \"([^\"]*)\"$")
    public void użytkownikWprowadzaNazwęUżytkownikaIHaslo(String login, String haslo) {
        System.out.println("Użytkownik podaje login: "+ login + " oraz hasło: " + haslo);
        this.username = login;
        this.password = haslo;
        logowanie.logIn(login, haslo);
    }

    @And("^Uzytkownik o nazwie \"([^\"]*)\" z haslem \"([^\"]*)\" istnieje w bazie danych$")
    public void uzytkownikONazwieZHaslemIstniejeWBazieDanych(String login, String haslo) {
        logowanie.setUserInDataBase(login, haslo);

    }

}
