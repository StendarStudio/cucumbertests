import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MyStepdefs {

    @Before
    public void setUp(){
        System.out.println("Metoda wykonana przed scenariuszem");
    }

    @Given("Konto z saldem 200zł")
    public void kontoZSaldem() {
        System.out.println("Metoda GIVEN konto z saldem \n");
    }

    @When("Użytkownik próbuje wypłacić 300 zł")
    public void wyplataZKonta(){
        System.out.println("Metoda WHEN wypłata z konta \n");
    }

    @Then("Saldo nie zmieni się i wynosi 200 zł")
    public void sprawdzenieSalda(){
        System.out.println("Metoda THEN sprawdzenie salda \n");

    }
    @After
    public void tearDown() {
        System.out.println("Metoda wykonana po scenariuszu");
    }

}
