@qa
Feature: Wypłata pieniędzy z bankomatu

  Scenario: Próba wypłacenia większej ilości gotówki niż saldo konta

    Given Konto z saldem 200zł
    When Użytkownik próbuje wypłacić 300 zł
    Then Saldo nie zmieni się i wynosi 200 zł