@dev
Feature: Dodawanie użytkownika

  Scenario Outline: Dodawanie użytkownika z poprawnymi danymi

    Given Użytkownik jest na stronie formularza dodawania nowego użytkownika

    When Użytkownik wprowadza poprawne dane do formularza
      |Imie|<imie>|
      |Nazwisko|<nazwisko>|
      |Miasto|<miasto>|
      |Ulica|<ulica>|
    Then Informacja dodaniu użytkownika pojawia się na ekranie

    Examples:
    |imie|nazwisko|miasto|ulica|
    |Tomasz|Kot|Warszawa|Warszawska|
    |Zdzislaw|Python|Wroclaw|Wrocławska|