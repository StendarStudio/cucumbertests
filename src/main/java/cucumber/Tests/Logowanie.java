package cucumber.Tests;

public class Logowanie {

    private String currentUsername;
    private String currentPassword;
    private String msg;
    private boolean isLoggedIn;

    public void setUserInDataBase(String userName, String password) {
        this.currentUsername = userName;
        this.currentPassword = password;
        }

        public boolean logIn(String userName, String password) {
            if (userName.equals(currentUsername) && password.equals(currentPassword)) {
                this.msg = "Udane logowanie";
                this.isLoggedIn = true;
            } else {
                this.msg = "Nieudane logowanie";
                this.isLoggedIn = false;

            } return isLoggedIn;
        }

        public String getMsg() {
        return msg;
        }

        public boolean isLoggedIn() {
        return isLoggedIn;
        }

        public String getCurrentUsername() {
        return currentUsername;
        }

        public String getCurrentPassword() {
        return currentPassword;
        }

}
