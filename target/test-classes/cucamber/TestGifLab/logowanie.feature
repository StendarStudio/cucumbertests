Feature: Logowanie do aplikacji
  W tym pliku będziemy testować logowanie użytkownika do aplikacji .
  Sekcja ta jest traktowana jako opis i nie wpływa na wykonanie testu.

  Background:
    #given jest warunkiem wstępnym przed wykonaniem testu
    Given Użytkownik jest na stronie logowania

  Scenario Outline: Użytkownik podaje poprawne dane logowania.
    And Uzytkownik o nazwie "<login>" z haslem "<haslo>" istnieje w bazie danych
    #when określa akcję, która zostanie wykonana
    When Użytkownik wprowadza nazwę użytkownika "<login>" i haslo "<haslo>"
    #and opisuje dodatkową akcje lub warunek
    And Użytkownik klika w przycisk "Zaloguj"
    #then opisuje wynik poprzednich kroków
    Then Użytkownik zostaje zalogowany na stronę domową aplikacji
    And Informacja o udanym logowaniu zostanie wyświetlona

    Examples:
    |login|haslo|
    |login1|haslo1|
    |login2|haslo2|

  Scenario: Użytkownik podaje błędne hasło
    And Uzytkownik o nazwie "login2" z haslem "haslo" istnieje w bazie danych
    When Użytkownik wprowadza nazwę użytkownika "login2" i haslo "zlehaslo"
    But Podane haslo jest błędne
    And Użytkownik klika w przycisk "Zaloguj"
    Then Użytkownik nie zostaje przeniesiony na stronę domową aplikacji
    And Informacja o nieudanym logoweaniu


 # Scenario: Użytkownik podaje błędną nazwę
 #   Given Użytkownik jest na stronie logowania


 # Scenario: Użytkownik podaje błędne hasło i nazwę
 #  Given Użytkownik jest na stronie logowania